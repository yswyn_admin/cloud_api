package com.cloud.api.log.controller;

import com.cloud.api.common.result.R;
import com.cloud.api.log.entity.CloudApiLogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log/all")
public class CloudApiLogController {

    @Autowired
    private MongoTemplate mongoTemplate;



    @GetMapping("/getLogList")
    public R getLogList(){
        List<CloudApiLogEntity> studentList = mongoTemplate.findAll(CloudApiLogEntity.class);
        return R.ok(studentList);
    }
}
