package com.cloud.api.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.config.EnableMongoAuditing;


/**
 * @Author 小坏
 * @Date 2022/10/29 14:44
 * @Version 1.0
 * @program: 日志系统
 */
@EnableMongoAuditing
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.cloud.api")
public class ApiLogApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiLogApplication.class, args);
    }
}