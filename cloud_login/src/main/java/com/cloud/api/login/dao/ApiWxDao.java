package com.cloud.api.login.dao;

import com.cloud.api.login.entity.ApiWxEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-08 22:39:39
 */
@Mapper
public interface ApiWxDao extends BaseMapper<ApiWxEntity> {
	
}
