package com.cloud.api.login.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-17 22:54:39
 */
@Data
@TableName("api_user")
public class ApiUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	/**
	 * 昵称
	 */
	private String nickName;
	/**
	 * 头像
	 */
	private String avatarUrl;
	/**
	 * openid
	 */
	private String openId;
	/**
	 * 性别
	 */
	private String gender;
	/**
	 * 省份
	 */
	private String province;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 年月
	 */
	private String year;

	private String type;
	private String phone;
	private Date createTime;


}
