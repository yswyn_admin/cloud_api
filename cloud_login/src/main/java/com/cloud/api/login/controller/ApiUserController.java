package com.cloud.api.login.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;


import com.cloud.api.common.result.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.api.login.entity.ApiUserEntity;
import com.cloud.api.login.service.ApiUserService;
import com.cloud.api.common.utils.PageUtils;


/**
 * @author å°å
 * @email 142277147@qq.com
 * @date 2022-11-17 22:54:39
 */
@RestController
@RequestMapping("login/apiuser")
public class ApiUserController {
    @Autowired
    private ApiUserService apiUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = apiUserService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id) {
        ApiUserEntity apiUser = apiUserService.getById(id);

        return R.ok().put("apiUser", apiUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ApiUserEntity apiUser) {
        apiUser.setCreateTime(new Date());


        ApiUserEntity openId = apiUserService.getOpenId(apiUser.getOpenId());
        if (openId == null) {
            apiUserService.save(apiUser);
            return R.ok();
        }
        return R.ok();

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ApiUserEntity apiUser) {
        apiUserService.updateById(apiUser);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids) {
        apiUserService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
