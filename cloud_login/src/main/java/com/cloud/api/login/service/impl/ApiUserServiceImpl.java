package com.cloud.api.login.service.impl;

import com.cloud.api.common.result.R;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.api.common.utils.PageUtils;
import com.cloud.api.common.utils.Query;

import com.cloud.api.login.dao.ApiUserDao;
import com.cloud.api.login.entity.ApiUserEntity;
import com.cloud.api.login.service.ApiUserService;


@Service("apiUserService")
public class ApiUserServiceImpl extends ServiceImpl<ApiUserDao, ApiUserEntity> implements ApiUserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ApiUserEntity> page = this.page(
                new Query<ApiUserEntity>().getPage(params),
                new QueryWrapper<ApiUserEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public R getList() {
        List<ApiUserEntity> apiUserEntities = this.baseMapper.selectList(null);
        return null;
    }

    @Override
    public ApiUserEntity getOpenId(String openId) {
        ApiUserEntity apiUser = this.baseMapper.selectOne(new QueryWrapper<ApiUserEntity>().eq("open_id", openId));
        return apiUser;
    }

}