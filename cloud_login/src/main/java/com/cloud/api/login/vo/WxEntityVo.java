package com.cloud.api.login.vo;

import lombok.Data;

@Data
public class WxEntityVo {
    private String session_key;
    private String openid;
    private String unionid;
}
