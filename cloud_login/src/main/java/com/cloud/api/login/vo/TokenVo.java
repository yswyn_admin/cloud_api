package com.cloud.api.login.vo;

import lombok.Data;

@Data
public class TokenVo {

    private String access_token;
    private String openid;

}
