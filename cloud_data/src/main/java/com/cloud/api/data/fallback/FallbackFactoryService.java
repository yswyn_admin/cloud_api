package com.cloud.api.data.fallback;

import com.alibaba.fastjson.JSONObject;
import com.cloud.api.data.feign.DataProductionFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;


/**
 * 第一实现 FallbackFactory
 * 第二步 DataProductionFeignService
 *
 */
@Slf4j
@Component
public class FallbackFactoryService implements FallbackFactory<DataProductionFeignService> {

    @Override
    public DataProductionFeignService create(Throwable cause) {
        log.error("服务异常。。。。。。。。。。。。。。。。。。。" + cause);
        return new DataProductionFeignService() {
            @Override
            public String getProduction(String id) {
                JSONObject data = new JSONObject();
                data.put("code","414");
                data.put("message","Fallback回滚，远程调用失败");
                return data.toJSONString();
            }

        };
    }
}