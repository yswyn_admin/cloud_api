package com.cloud.api.data.feign;


import com.cloud.api.data.fallback.FallbackFactoryService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "cloud-production",fallbackFactory = FallbackFactoryService.class)
public interface DataProductionFeignService {


    @GetMapping("/production/all/getMysql")
    String getProduction(@RequestParam("id") String id);


}
