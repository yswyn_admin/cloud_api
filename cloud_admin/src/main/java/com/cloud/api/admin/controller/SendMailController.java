package com.cloud.api.admin.controller;

import com.cloud.api.admin.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/mail")
public class SendMailController {

    @Autowired
    private SendMailService sendMailService;


    @GetMapping("/sendMail")
    public void SendMail(String msg) {
        sendMailService.sendMail(msg);
    }
}
