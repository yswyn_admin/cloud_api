package com.cloud.api.admin.service;

public interface SendMailService {

    public void sendMail(String msg);
}
