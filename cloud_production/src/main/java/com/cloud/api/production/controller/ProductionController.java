package com.cloud.api.production.controller;

import com.cloud.api.common.result.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/production/all")
public class ProductionController {


    @GetMapping("/getMysql")
    public R getMysql(@RequestParam("id") String id) throws InterruptedException {
        Thread.sleep(1000L);
        System.out.println("我是生产者" + id);
        return R.ok(id);
    }
}
