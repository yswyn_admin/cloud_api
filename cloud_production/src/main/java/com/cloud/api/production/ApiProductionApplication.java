package com.cloud.api.production;

import com.cloud.api.production.conf.DefaultFeignLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author 小坏
 * @Date 2022/10/29 14:44
 * @Version 1.0
 * @program: 消息生产
 */

@EnableFeignClients(defaultConfiguration = DefaultFeignLog.class)
@SpringBootApplication(scanBasePackages = "com.cloud.api")
public class ApiProductionApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiProductionApplication.class, args);
    }
}