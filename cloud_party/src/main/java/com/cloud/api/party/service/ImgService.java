package com.cloud.api.party.service;

import com.cloud.api.common.result.R;
import com.cloud.api.party.utils.OSSEntity;
import com.cloud.api.party.utils.ToImages;
import org.springframework.web.multipart.MultipartFile;

public interface ImgService {
R toImages(ToImages toImages);

    R toFilePutLoad(MultipartFile file);

    R toBasePutLoad(OSSEntity oss);
}