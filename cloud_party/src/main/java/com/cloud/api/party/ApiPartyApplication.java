package com.cloud.api.party;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author 小坏
 * @Date 2022/10/29 14:44
 * @Version 1.0
 * @program: 三方服务
 */
@EnableFeignClients
@SpringBootApplication(scanBasePackages = "com.cloud.api")
public class ApiPartyApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiPartyApplication.class, args);
    }
}