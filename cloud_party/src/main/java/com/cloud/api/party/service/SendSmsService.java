package com.cloud.api.party.service;


import com.cloud.api.common.result.R;
import com.cloud.api.party.utils.SendCodeVo;

public interface SendSmsService {

    //获取验证码
    R sendCheckCode(String phone);


    //验证验证码
    R sendCheckPhoneCode(SendCodeVo vo);
}
